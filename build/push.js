const commandLineArgs=require('../lib/command-line-args'),
log=require('../lib/log'),
fs=require('fs'),
exec=require('../lib/exec');
commandLineArgs(this, [
	{
		name: 'commit',
		type: String,
		defaultValue: 'Changes',
		description: 'Message to commit'
	},
	{
		name: 'repository',
		alias: 'r',
		type: String,
		defaultValue: 'origin'
	},
	{
		name: 'branch',
		alias: 'b',
		type: String,
		defaultValue: 'master'
	}
]);
function push(){
	exec(
		'git add .',
		`git commit -m "${process.arguments.commit}"`,
		`git push -u ${process.arguments.repository} ${process.arguments.branch}`
	);
}
var bindfs=`${process.cwd()}/bindfs.bash`;
if(fs.existsSync(bindfs)){
	exec(bindfs).then(push);
}
else push();