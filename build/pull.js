const commandLineArgs=require('../lib/command-line-args'),
log=require('../lib/log'),
exec=require('../lib/exec');
commandLineArgs(this, [
	{
		name: 'repository',
		alias: 'r',
		type: String,
		defaultValue: 'origin'
	},
	{
		name: 'branch',
		alias: 'b',
		type: String,
		defaultValue: 'master'
	}
]);
exec(
	//`git stash`,
	`git pull ${process.arguments.repository} ${process.arguments.branch}`
);